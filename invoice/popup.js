// This callback function is called when the content script has been
// injected and returned its results

var BASE_URL = "http://invoice.agrostar.in";

function onPageDetailsReceived(pageDetails)  {
    var shipments=JSON.parse(pageDetails.shipmentData);
    shipments.forEach(function(shipment){
      var _trow='<tr data-shipment='+shipment.code+'><th>'+
          '<input type="radio" name="shipmentRadio" data-codes='+shipment.code+','+shipment.soi+'>'+
        '</th>'+
        '<td>'+shipment.code+'</td>'+
        '<td>'+shipment.soi+'</td></tr>';
        $('#shipment-table').append(_trow);
    });
}

document.getElementById("change-facility").onclick =  function (event) {
  localStorage.clear();
  window.close();
};

//Set Facility
function _setFacility(){
  if(!localStorage.getItem('FacilityName')){
    _facility = prompt("Enter Facility (1/2/3) : \n 1. GJ01/SeedLogic Trading LLP \n 2. RJ01 SeedLogic Trading LLP/RJ01 \n 3. MH01 SeedLogic Trading LLP/MH01");
    switch(_facility){
      case "1":
        localStorage.setItem('FacilityCode','AGROSTARWAREHOUSE');
        localStorage.setItem('FacilityName', "Gujarat");
        break;
      case "2":
        localStorage.setItem('FacilityCode','RJ01');
        localStorage.setItem('FacilityName', "Rajasthan");
        break;
      case "3":
        localStorage.setItem('FacilityCode','MH01');
        localStorage.setItem('FacilityName', "Maharashtra");
        break;
    }
  } 
  var facilityName = localStorage.getItem('FacilityName');
  document.getElementById("facilityName").innerText = facilityName;
  
}

//Get selected shipments
function _getSelecetdShipments(_printType){
  var _codes,_pkgCode,_soi,_facilityCode,_data={};
  _facilityCode=localStorage.getItem('FacilityCode');

  _codes=$('input[name=shipmentRadio]:checked').data('codes').split(',');
  _pkgCode=_codes[0];
  _soi=_codes[1];
  _data={
    pkgCode : _pkgCode,
    soi : _soi,
    facility : _facilityCode
  };
  _sendForPrint(_data,_printType);
}

// Function Show Error
function _showError(_errorMsg) {
  var error= '<div class="alert alert-danger" role="alert"> \
                <button type="button" class="close" id="close-alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> \
                  '+ _errorMsg +' \
              </div>' ;

  $("#error-msg").html(error);
  setTimeout(function(){
    $("#error-msg").html('');
  },3000);

  //close alert handler
  $("#close-alert").click(function(){
    $("#error-msg").html('');
  });

}

function encodeQueryData(url , data) {
   let ret = [];
   for (let d in data)
     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
   return ""+url + ret.join('&')+ "";
}

function _sendForPrint(_data,_printType){
    var data = {"orderId":_data.soi ,"packageId": _data.pkgCode ,"facilityId":_data.facility};
    var url = BASE_URL + "/node/invoice/?";
     window.open(encodeQueryData(url,data));
}

function _getAutocompleteArry() {
  var _array=[];
  $('#shipment-table tr').each(function(){
    _array.push($(this).data('shipment'));
  });
  return _array;
}

// When the popup HTML has loaded
window.addEventListener('load', function(evt) {
    //set facility
    _setFacility();

    setTimeout(function(){
      $( "#shipment-id" ).autocomplete({
        source: _getAutocompleteArry()
      });
    },300);

    //handler for print button
    $('#print-invoice').click(function(){
      _getSelecetdShipments();
    });

    //Filter on keyup
    $( "#shipment-id" ).on('autocompleteclose',function(e) {
        e.preventDefault();
        var shipment=$(this).val();
        $("#shipment-table").find("tr th input").prop('checked',false);
        $("#shipment-table").find("[data-shipment='" + shipment + "'] th input").prop('checked',true);
    })

    //on enter handler
    $(document).keypress(function(e) {
      if(e.which == 13) {
        _getSelecetdShipments();
      }
    });

    // Get the event page
    chrome.runtime.getBackgroundPage(function(eventPage) {
        // Call the getPageInfo function in the event page, passing in
        // our onPageDetailsReceived function as the callback. This injects
        // content.js into the current tab's HTML
        eventPage.getPageDetails(onPageDetailsReceived);
    });
});
