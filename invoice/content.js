
var data=[];
$("#detailView .detail-row .detail-row-head").each(function(){
  _soi=window.location.href.indexOf("/order/shipments") > -1 ? $('.child-breadcump').text().split('#').join('') : $(this).find(".detailViewField:nth-child(3) value").text();
  _code=($(this).find(".detailViewField:nth-child(2) value").text());
  //_soi=($(this).find(".detailViewField:nth-child(3) value").text());
  data.push({
    code : _code,
    soi : _soi
  })
});

// Send a message containing the page details back to the event page
chrome.runtime.sendMessage({'shipmentData': JSON.stringify(data)});
