var BASE_URL = "http://invoice.agrostar.in";

document.getElementById("change-facility").onclick =  function (event) {
  localStorage.clear();
  window.close();
};

//Set Facility
function _setFacility(){
  if(!localStorage.getItem('FacilityName')){
    _facility = prompt("Enter Facility (1/2/3) : \n 1. GJ01/SeedLogic Trading LLP \n 2. RJ01 SeedLogic Trading LLP/RJ01 \n 3. MH01 SeedLogic Trading LLP/MH01");
    switch(_facility){
      case "1":
        localStorage.setItem('FacilityCode','AGROSTARWAREHOUSE');
        localStorage.setItem('FacilityName', "Gujarat");
        break;
      case "2":
        localStorage.setItem('FacilityCode','RJ01');
        localStorage.setItem('FacilityName', "Rajasthan");
        break;
      case "3":
        localStorage.setItem('FacilityCode','MH01');
        localStorage.setItem('FacilityName', "Maharashtra");
        break;
    }
  } 
  var facilityName = localStorage.getItem('FacilityName');
  document.getElementById("facilityName").innerText = facilityName;
}

//Get selected shipments
function _getShipmentId() {
  var _facilityCode, _data = {};
  _facilityCode = localStorage.getItem('FacilityCode');

  _pkgCode = $('#shipment-id').val();

  _data = {
    pkgCode: _pkgCode,
    facility: _facilityCode
  };
  _sendForPrint(_data);
}

function encodeQueryData(url , data) {
   let ret = [];
   for (let d in data)
     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
   return ""+url + ret.join('&')+ "";
}

function _sendForPrint(_data,_printType){
    var data = {
      "packageId": _data.pkgCode,
      "facilityId": _data.facility
    };
    var url = BASE_URL + "/node/challan/?";
     window.open(encodeQueryData(url,data));
}

// When the popup HTML has loaded
window.addEventListener('load', function(evt) {
  //set facility
  _setFacility();

  //handler for print button
  $('#print-challan').on('click', function(e) {
    e.preventDefault();
    _getShipmentId();
  });

//   //TODO: Need to use Form Submit instead of onClick
//   document.getElementById('print-challan').onclick = function(event) {
//       event.preventDefault();

//   }

  //handler for input field change
  // $( "#shipment-id" ).on('change',function() {
  //   debugger;
  //   _getShipmentId();
  // });

  //handler for enter key
  $(document).keypress(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      _getShipmentId();
    }
  });
});

// All TODOs
// 1. Remove jquery support
// 2. Remove Bootstrap support
